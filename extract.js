var fs = require('fs');

fs.readFile('annualmeans.txt', 'utf8', (err, content) => {
  var result = content.split("\n").filter(n => n).slice(7);
  let years = [],
      nH = [],  
      sH = [];
  //skip till first letter doesn't start with a number 
  for (var i = 0; i < result.length; i++) {
    if ((Number(result[i][0]))) {
      let splitResult = result[i].split(' ');
      splitResult = splitResult.filter(n => n);
      years.push(splitResult[0]);
      nH.push(splitResult[2]);
      sH.push(splitResult[3]);
    } 
  }
  var obj = {};
  obj["years"] = years;
  obj["nH"] = nH;
  obj["sH"] = sH;

  fs.writeFileSync('data.json', JSON.stringify(obj), 'utf8');
});
