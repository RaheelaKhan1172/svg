'use strict';

function test() {
  var xml = new XMLHttpRequest();
  xml.overrideMimeType('application/json');
  xml.open('GET', 'data.json', true);
  xml.onreadystatechange = function() {
    if (xml.readyState === 4 && xml.status === 200) {
      gotData(xml.response);
    }
  };

  xml.send();
}

function gotData(data) {
  let result = JSON.parse(data);
  let concat = [...result.nH, ...result.sH];
  let min = findX(concat, (x,y) => Number(x) < Number(y));
  let max = findX(concat, (x, y) => Number(x) > Number(y)); 
  let years = makeRange([result.years[0], result.years[result.years.length-1]], 9);
  /* make x line and ticks */
  makeX(years);

  plotY(min,max);
  min = min / 100;
  max = max / 100;

  for (let i = 0; i < result.nH.length;i++) {
    result.nH[i] = result.nH[i] / 100;
    result.sH[i]  = result.sH[i] / 100;
  }

  plotData(result.years, result.nH, result.sH , min , max); 
}

let convenientGlobalObj = {};
function plotData(years, north, south, min, max) {
  let xScale = scaleWithRange([years[0], years[years.length-1]], [65,810]);
  let yScale = scaleWithRange([-1, 1.4] , [0, 400]); //input mapped to pixels
  let svg = document.getElementsByTagName('svg')[0];

  let polyLine = document.createElementNS("http://www.w3.org/2000/svg", "polyline");
  let nH = "";
  let sH = "";
  let scaledYear = null;
  let northCircle = null;
  let southCircle = null;

  for (let i = 0; i < years.length; i++) {
    southCircle = document.createElementNS("http://www.w3.org/2000/svg", "circle");
    northCircle = document.createElementNS("http://www.w3.org/2000/svg", "circle");
    scaledYear = xScale(years[i]);
    nH += `${scaledYear},${400 - yScale(north[i])} `;
    sH += `${scaledYear},${400 - yScale(south[i])} `;
    northCircle.setAttribute('cx', scaledYear);
    northCircle.setAttribute('cy', 400 - yScale(north[i]));
    southCircle.setAttribute('cx', scaledYear);
    southCircle.setAttribute('cy', 400 - yScale(south[i]));
    northCircle.setAttribute('r', 2);
    southCircle.setAttribute('r', 2);
    southCircle.setAttribute('fill', 'blue');
    northCircle.setAttribute('fill', 'red');

    northCircle.setAttribute('id', `n${i}`);
    southCircle.setAttribute('id', `s${i}`);
    convenientGlobalObj[`n${i}`] = { year: years[i], temp: yScale(north[i]) };
    convenientGlobalObj[`s${i}`] = { year: years[i], temp: yScale(south[i]) };
    svg.appendChild(northCircle);
    svg.appendChild(southCircle);
    // ^ yikes 0_0
    
  }
 
  let southLine = document.createElementNS("http://www.w3.org/2000/svg", "polyline");
  southLine.setAttribute("points", sH);
  polyLine.setAttribute("points", nH);
  polyLine.setAttribute("fill", "none");
  polyLine.setAttribute("stroke", "red");
  
  southLine.setAttribute('fill', 'none');
  southLine.setAttribute('stroke', 'blue');

  svg.appendChild(polyLine);
  svg.appendChild(southLine);

}

function plotY(min,max) {
  let svg = document.getElementsByTagName('svg')[0];
  console.log(min, max);
  let text = [];
 
   for (var i = -10; i <= 14; i += 2) {
   text.push((i / 10)); 
  }
 
  let scale = scaleWithRange([text[0], text[text.length-1]], [0 ,400]);
 

  for (var i = 0; i < text.length; i++) {
    let yTick = document.createElementNS('http://www.w3.org/2000/svg', 'line');
    let scaleValue = scale(text[i]);
    yTick.setAttribute('x1', 60);
    yTick.setAttribute('x2', 65);
    yTick.setAttribute('y1', 400 - scaleValue);
    yTick.setAttribute('y2', 400 - scaleValue);
    yTick.setAttribute('stroke', 'black');


    let t = document.createElementNS("http://www.w3.org/2000/svg", "text");
    t.setAttribute('x', 30);
    t.setAttribute('y', (400 - scaleValue === 0) ? 11 : 400 - scaleValue);
    t.textContent = text[i];
    svg.appendChild(t);
    svg.appendChild(yTick);
  }
}

function makeX(years) {
  let svg = document.getElementsByTagName("svg")[0];
  let height = svg.height.baseVal.value;
  let width = svg.width.baseVal.value;

  let y = height - 100;
  let x = 65;
  
  let xLine = document.createElementNS("http://www.w3.org/2000/svg", "line");
  xLine.setAttribute('x1', x);
  xLine.setAttribute('x2', 810);
  xLine.setAttribute('y1', y);
  xLine.setAttribute('y2', y);
  xLine.setAttribute("stroke", "black");
  svg.appendChild(xLine);


  let yLine = document.createElementNS("http://www.w3.org/2000/svg", "line");
  yLine.setAttribute('x1', x);
  yLine.setAttribute('x2', x);
  yLine.setAttribute('y1', 0);
  yLine.setAttribute('y2', y);
  yLine.setAttribute('stroke', 'black');
  svg.appendChild(yLine);

  /*
    got done appending lines, now need to add x Axis ticks, and y Axis ticks as well,
    then tsart plotting
  */
  let scaled = scaleWithRange(years, [65, 810]);
  
  for (let i = 0; i < years.length; i++) {
    let elem = document.createElementNS("http://www.w3.org/2000/svg", "line");
    let xVal = scaled(years[i]);
    
    elem.setAttribute('x1', xVal);
    elem.setAttribute('x2', xVal);
    elem.setAttribute('y1', (y));
    elem.setAttribute('y2', y + 5);
    elem.setAttribute('stroke' , 'black');
    
    let t = document.createElementNS("http://www.w3.org/2000/svg", "text");
    t.setAttribute('x', xVal - 10);
    t.setAttribute('y', y + 20 );
    t.textContent = years[i];

    svg.appendChild(t);
    svg.appendChild(elem);
  
  }
}

function scale(min, max, x) {
  return (x / max - min / max) + min;
}

function scaleWithRange(domain, range) {
  // output = scaling_factor * input + offset;
  let largestInDomain = findX(domain, (x, y) => x > y);
  let minInDomain = findX(domain, (x,y) => x < y);

  let largestInRange = findX(range, (x, y) => x > y);
  let minInRange = findX(range, (x, y) => x < y);
  
  let diffInDomain = largestInDomain - minInDomain;
  let rangeDiff = largestInRange - minInRange;
  let scalingFactor =  rangeDiff / diffInDomain;
  let offset = minInRange - minInDomain * scalingFactor;
  console.log("offset", scalingFactor); 
  return (n) => {
    return (scalingFactor * n + offset);
  }
}

/* make some ticks */
function makeRange(range, ticks) {
  let [lower,upper] = [range[0], range[1]];
  let newRange = upper - lower;
  let tickRange = getTickRange(newRange, ticks);  
  lower = tickRange * Math.round(lower / tickRange);
  upper = tickRange * Math.round(( 1 + upper ) / tickRange);
  let years = [];
  
  for (var i = 0; i < ticks; i++) {
    years.push(lower);
    lower += tickRange;
  }

  return years;

}

function getTickRange(range, tick) {
  let tickRange = range / tick;
  tickRange = tickRange / Math.pow(tick, 2);
  var ticks = [];
  for (var i = 0; i <= 10; i++) {
    
    ticks.push( i / 10 );
    if (i === 2) {
      ticks.push( 0.25);
    }

    if (i === 7) {
      ticks.push(0.75);
    }
  }
  let curRange = null;
  for (var i = 0; i < ticks.length; i++) {
    if (ticks[i] >= tickRange) {
      curRange = ticks[i];
      break;
    }
  }

  return curRange * Math.pow(10,2);

}

function findX(arr, pred) {
  let target = arr[0];
  for (let i = 1; i < arr.length;i++) {
    if (pred(arr[i], target)) {
      target = arr[i];
    }
  }
  return target;
}

test();

function mouseOver(e) {
  if (e.target.tagName.toLowerCase() === 'circle') {
    console.log(convenientGlobalObj[e.target.id], e.target.cx);
    let toolTip = document.getElementById("tool-tip");
    toolTip.innerHTML = `<p>Year: ${convenientGlobalObj[e.target.id].year} </p> <p> Avg Temp: ${convenientGlobalObj[e.target.id].temp} </p>`;
    toolTip.style.top =  `${e.target.cy.baseVal.value}px`;
    toolTip.style.left =  `${e.target.cx.baseVal.value}px`;
    toolTip.style.display = 'block';
    console.log('e', e.target);
  } else {
  }
}

document.getElementById("svg").addEventListener('mouseover', mouseOver, false);
